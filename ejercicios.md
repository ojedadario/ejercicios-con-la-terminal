# Ejercicios con la terminal. MPVD. Módulo I.

*Leyenda*

*MV: máquina virtual*

*WS: Windows*

**Pregunta 1. Debemos actualizar el sistema operativo Ubuntu:**

-   **Sincronizar la base de datos local y el repositorio**
    
-   **Visualizar los programas a actualizar**
    
-   **Actualizar nuestro sistema operativo**
    

Respuesta (MV)

-   sudo apt update
    
-   apt list --upgradable
    
-   sudo apt upgrade
    

**Pregunta 2. Determinar con un comando cuál es nuestra posición en el árbol del usuario.**

Respuesta (MV)

-   pwd
    
-   /home/dojeda
    

Respuesta (WS)

-   pwd
    
-   /cygdrive/c/Users/dario
    

**Pregunta 3. Visualizar con un comando el árbol del usuario desde /home/nuestroUsuario/**

Respuesta (MV)

-   tree
    

Respuesta (WS)

-   ls
    

  
  
  

**Pregunta 4. Crear en /home/nuestroUsuario/ un folder llamado "ejercicios".**

Respuesta (MV)

-   mkdir ejercicios
    

Respuesta (WS)

-   mkdir ejercicios
    

**Pregunta 5. Verificar que lo hemos creado en el lugar correcto y su posición en el árbol.**

tree

ls

  

**Pregunta 6. Movernos a la carpeta "ejercicios" y verificar con el comando que estamos ahí.**

Respuesta (MV)

  

-   cd ejercicios
    
-   pwd
    
-   /home/dojeda/ejercicios
    

  

Respuesta (MV)

  

-   cd ejercicios
    
-   pwd
    
-   /cygdrive/c/Users/dario/ejercicios
    

**Pregunta 7. Creamos un archivo de texto dentro del folder "ejercicios", escribimos texto y lo salvamos adecuadamente con el nombre parte_1.**

  

Respuesta (MV)

  

-   Creo el archivo con nano y lo guardo con el nombre parte_1
    
-   Lo compruebo con el comando ls
    

  

Respuesta (WS)

  

-   Creo el archivo con nano y los guardo con el nombre parte_1
    
-   Los compruebo con el comando ls
    

  

**Pregunta 8. Retrocedemos a /home/nombreUsuario/ utilizando el comando de retroceso o bien el comando de path absoluto. Verificamos con un comando nuestra posición.**

Respuesta (MV)

  

-   cd
    
-   pwd
    
-   /home/dojeda
    

Respuesta (WS)

  

-   cd
    
-   pwd
    
-   /cygdrive/c/Users/dario
    

**Pregunta 9. Con el comando y los parámetros adecuados generamos un listado.**

  

Respuesta (MV)

  

-   ls
    

Respuesta (WS)

  

-   ls
    

**Pregunta 10. Con el comando anterior agregamos otro parámetro y visualizamos los archivos ocultos también.**

Respuesta (MV)

  

-   ls -a
    

Respuesta (WS)

  

-   ls -a
    

**Pregunta 11. Desde /home/nuestroUsuario/ creamos otro archivo de texto llamado "parte_2", escribimos un texto, lo salvamos.**

  

Respuesta (MV)

  

-   cd ejercicios
    
-   Creo el archivo con nano y lo guardo con el nombre parte_2
    
-   Lo compruebo con el comando tree
    

  

Respuesta (WS)

  

-   cd ejercicios
    
-   Creo el archivo con nano y lo guardo con el nombre parte_2
    
-   Lo compruebo con el comando ls
    

**Pregunta 12. Desde la raíz del usuario creamos un folder llamado "trabajos".**

  

Respuesta (MV)

  

-   mkdir trabajos
    

  

Respuesta (WS)

  

-   mkdir trabajos
    

**Pregunta 13. Desde la raíz del usuario creamos un archivo de texto plano llamado "parte_3", lo salvamos, salimos y verificamos que esté en lugar adecuado.**

Respuesta (MV)

-   cd trabajos
    
-   Creo el archivo con nano y los guardo con el nombre parte_3
    
-   Los compruebo con el comando tree
    

  

Respuesta (WS)

-   cd trabajos
    
-   Creo el archivo con nano y los guardo con el nombre parte_3
    
-   Los compruebo con el comando ls
    

  

**Pregunta 14. Desde la raíz del usuario abrimos los tres archivos creados, agregamos en cada uno de ellos nuestro nombre al final del texto, lo salvamos y salimos del editor.**

  

Respuesta (MV)

  

-   nano ejercicios/parte_1 → modifico y guardo el archivo con el mismo nombre.
    
-   nano ejercicios/parte 2 → modifico y guardo el archivo con el mismo nombre.
    
-   nano trabajos/parte_3 → modifico y guardo el archivo con el mismo nombre.
    

  

Respuesta (WS)

  

-   nano ejercicios/parte_1 → modifico y guardo el archivo con el mismo nombre.
    
-   nano ejercicios/parte 2 → modifico y guardo el archivo con el mismo nombre.
    
-   nano trabajos/parte_3 → modifico y guardo el archivo con el mismo nombre.
    

**Pregunta 15. Nos dirigimos al folder donde está ubicado el archivo "parte_3". Verificamos la ubicación con el comando adecuado.**

  

Respuesta (MV)

  

-   cd trabajos
    
-   pwd
    
-   /home/dojeda/trabajos
    

  

Respuesta (WS)

  

-   cd trabajos
    
-   pwd
    
-   /cygdrive/c/Users/dario/trabajos
    

**Pregunta 16. Con el comando adecuado copiamos el archivo "parte_3" en el folder donde están los otros dos archivos y verificamos con el comando "tree".**

  

Respuesta (MV)

  

-   mv parte_3 /home/dojeda/ejercicios
    

  

Respuesta (WS)

  

-   mv parte_3 /cygdrive/c/Users/dario/ejercicios
    

  

**Pregunta 17. Desde la actual posición en el árbol nos movemos al folder dónde están los tres archivos recién creados (ejercicios). Verificamos nuestra posición.**

  

Respuesta (MV)

  

-   cd /home/dojeda/ejercicios
    
-   pwd
    
-   /home/dojeda/ejercicios
    

  

Respuesta (WS)

-   cd /cygdrive/c/Users/dario/ejercicios
    
-   pwd
-   /cygdrive/c/Users/dario/ejercicios    

**Pregunta 18. Con el comando adecuado listamos los archivos del folder donde estamos ubicados.**

Respuesta (MV)

-   ls

Respuesta (WS)

-   ls
    
**Pregunta 19. Utilizamos el comando adecuado para leer los tres archivos simultáneamente, visualizaremos sus textos en líneas separadas (en el terminal).**

Respuesta (MV)

-   cat parte_1 parte_2 parte_3

Respuesta (WS)

-   cat parte_1 parte_2 parte_3 

**Pregunta 20. concatenamos los tres archivos y al nuevo archivo lo llamamos "textoTotal".**

Respuesta (MV)

-   cat parte_1 parte_2 parte_3 > textoTotal 

Respuesta (WS)

-   cat parte_1 parte_2 parte_3 > textoTotal
    
**Pregunta 21. listamos los archivos del folder y verificamos si esta "textoTotal".**

Respuesta (MV)

-   ls
    
Respuesta (WS)

-   ls

**Pregunta 22. Con el editor abrimos "textoTotal" y escribimos "trabajo finalizado". Lo salvamos y salimos.**

Respuesta (MV)

-   nano textoTotal
    
-   Modifico el archivo y lo guardo con el mismo nombre.
    
Respuesta (WS)

-   nano textoTotal
    
-   Modifico el archivo y lo guardo con el mismo nombre.
    
**Pregunta 23. Ahora borramos o removemos el folder "trabajos". **

Respuesta (MV)  

-   rm -r /home/dojeda/trabajos
    
Respuesta (WS)

-   rm -r /cygdrive/c/Users/dario/trabajos 

**Pregunta 24. Nos movemos desde nuestra posición actual a /home/nuestroUsuario/**

Respuesta (MV)

-   cd

Respuesta (WS)

-   cd
  
**Pregunta 25. Desde la raíz del usuario creamos un folder llamado "final".**

Respuesta (MV)

-   mkdir final

Respuesta (WS)  

-   mkdir final

**Pregunta 26. Copiamos el archivo "textoTotal", de donde está dicho archivo, al folder "final", utilizamos el path absoluto.**

Respuesta (MV)

-   mv textoTotal /home/dojeda/final

Respuesta (WS)

-   mv textoTotal /cygdrive/c/Users/dario/final

**Pregunta 27. Finalmente borramos el folder "ejercicios".**

Respuesta (MV)

-   rm -r /home/dario/ejercicios
    
Respuesta (WS)

-   rm -r /cygdrive/c/Users/dario/ejercicios
