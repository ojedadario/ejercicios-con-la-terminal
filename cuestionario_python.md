# Cuestionario Python

## 1. ¿Cómo tienes instalado Python? Podéis acompañarlo de la salida de which python, which python3, python --version, python3 --version

Lo instalé a través de la terminal en una de las primeras clases. 

Al ejecutar `which python` me devuelve: 

~~~
/usr/bin/python 
~~~

Si ejecuto `which python3`, muestra:

~~~ 
/usr/bin/python3
~~~

Con `python --version` compruebo qué versión tengo instalada:

~~~
Python 3.8.10
~~~

## 2. ¿Tienes Anaconda? ¿Lo usas fuera del máster o/y en el máster? Puedes acompañarlo de echo $PATH.

Tengo Anaconda y los he utilizado en el máster y también en mi trabajo.

## 3. ¿Ejecutas Python desde la terminal? Con la salida de which hazle un ls -la a esa ruta. Aquí me interesa saber si ejecutas Jupyter desde la terminal, desde la aplicación gráfica de Anaconda o cómo. 

Apenas he ejecutado Python desde la terminal. Y Jupyter lo ejecuto desde la aplicación gráfica de Anaconda, como practicamos con Martín Nadal en las clases.

## 4. ¿Usas Collab? ¿Solo collab o como una aplicación de terceros más?

No sé qué es collab.

## 5. ¿Sabes que es pip? Puedes acompañarlo de pip --version y pip3 --version, which pip y which pip3. 

Pip es el gestor de librerías de Python. Lo utilizamos para instalar y actualizar los paquetes.

Si lanzo `which pip`, me devuelve:

~~~
/usr/local/bin/pip
~~~

Si ejecuto `pip --version`, lo que se muestra es:

~~~
pip 21.3.1 from /usr/local/lib/python3.8/site-packages/pip (python 3.8)
//
~~~

## 6. ¿Usas pip, condas u otro método?

He intentando utilizar pip para instalar pandas y reproducir en la terminal las prácticas que realizamos con Martín Nadal utilizando Anaconda, pero me da fallo y no me deja instalar ningún paquete.
